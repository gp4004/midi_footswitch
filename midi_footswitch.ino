#include <MIDIUSB.h>

#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);
/*
   0x03C
   https://github.com/duinoWitchery/hd44780
   exemples hd44780 / ioclass / hd44780_I2CExp / I2CexpDiag => scanner I2C
*/
  
#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2

#define LOGO16_GLCD_HEIGHT 16
#define LOGO16_GLCD_WIDTH  16

const int BUTTON_BANK = 5;
const int BUTTON_1 = 6;
const int BUTTON_2 = 7;
const int BUTTON_PANIC = 8;

int lastButton1State = 0;
int lastButton2State = 0;
int lastButtonPanicState = 0;
int lastButtonBankState = 0;

int myDelay = 30;

int button1State = 0;
int button2State = 0;
int buttonPanicState = 0;
int buttonBankState = 0;

int notes[] = {0, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32 }; // & so on...
String song_name[] = {"Dummy song", "Song 1", "Song 2", "Song 3"};
int notePanic = 100;

/*bank / song
  1  Song 1
  2  Song 2
  3  Song 3
*/
int nb_songs = 3;
int song_order[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};


int i = 1;
int bank = song_order[i]; // change to first song N°




void setup()
{
  delay(2000);
  pinMode(BUTTON_1, INPUT_PULLUP);
  pinMode(BUTTON_2, INPUT_PULLUP);
  pinMode(BUTTON_PANIC, INPUT_PULLUP);
  pinMode(BUTTON_BANK, INPUT_PULLUP);
  Serial.begin(115200); // communication speed
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr (0x03C)
  display.display();        // show splashscreen  // Display Adafruit logo
  delay(1000);
  display.clearDisplay();   // clears the screen and buffer
  printText(song_name[bank]);
}


void loop()
{
  button1State = digitalRead(BUTTON_1);
  button2State = digitalRead(BUTTON_2);
  buttonPanicState = digitalRead(BUTTON_PANIC);
  buttonBankState = digitalRead(BUTTON_BANK);


  if (button1State != lastButton1State) {
    if (button1State == LOW) { 
      noteOn(0, notes[bank], 100);
      delay(10);
      noteOff(0, notes[bank], 100);
    }
    delay(myDelay);
  }
  lastButton1State = button1State;


  if (button2State != lastButton2State) {
    if (button2State == LOW) {
      noteOn(0, notes[bank] + 40, 100);
      delay(10);
      noteOff(0, notes[bank] + 40, 100);
    }

    delay(myDelay);
  }
  lastButton2State = button2State;

  if (buttonPanicState != lastButtonPanicState) {
    if (buttonPanicState == LOW) {
      noteOn(0, 100, 100);
      delay(10);
      noteOff(0, 100, 100);
    }
    delay(myDelay);
  }
  lastButtonPanicState = buttonPanicState;


  if (buttonBankState != lastButtonBankState) {
    if (buttonBankState == LOW) {
      i += 1;
      if (i > nb_songs) {
        i = 1;
      }
      Serial.println(bank);
      bank = song_order[i];
      printText(song_name[bank]);
    }
    delay (myDelay);
  }
  lastButtonBankState = buttonBankState;
}


void noteOn(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOn = {0x09, 0x90 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOn);
  MidiUSB.flush();
}


void noteOff(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOff = {0x08, 0x80 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOff);
  MidiUSB.flush();
}


void printText(String bank)
{
  display.clearDisplay();
  display.setTextSize(4);
  display.setTextColor(WHITE);
  display.setCursor(0, 0);
  display.println(bank);
  display.display();
}
