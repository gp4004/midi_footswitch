////////////////////////////////////////////
// VARS
thickness = 2;
length = 140;
width = 100;
height = 26.5; //24.5, 
switch = 13; //for 11.7 diam
jack = 6.1; //for 5.7 diam
jack2 = 8;
$fn = 100;
diam_plate_screw_hole = 3.2;
screw_cylinder = 6;
screw_cylinder_inside = 2.9;
arduino_length = 33;
arduino_width = 18.2;
block_width = 10;
block_length = 5;
block_height = 4;

//cable diameter
cable_diam = 3;

////////////////////////////////////////////


////////////////////////////////////////////
//GENERATE

    //plate();
    box();


////////////////////////////////////////////
// MODULES

module clear()
{
    cube ([77, 100, 30]);
    translate ([60,0,0]) cube ([50, 60, 30]);
    translate ([60,50,10]) cube ([50, 60, 30]);
    translate ([103, 0,0]) cube ([80, 100, 30]);
}

module jack_hole()
{
    color ([1,0,0]) 
    translate ([length / 2, width + 0.1, 8])    
    rotate ([90,90,0])
    union()
    {
        cylinder (r = jack / 2, h = thickness);
        translate ([0,0,1.5]) cylinder (r = jack2 / 2, h = thickness);
    }
}

module arduino()
{
    //translate ([ (length - arduino_width ) / 2, width - arduino_length - thickness -50, thickness ]) arduino_block();
    translate ([ (length - arduino_width ) / 2 - 10, thickness, thickness ]) arduino_block();
}

module arduino_block()
{
    difference(){
        arduino_case();
        arduino_card();
    }
}

module arduino_stop_block()
{
    difference()
    {
        color ([0, 0, 1]) translate ([(block_width - thickness) /2  , - block_length, 0]) cube ([block_width, block_length, block_height]);
    
        translate ([(block_width - thickness / 2)   , - block_length / 2, 0]) cylinder (r=1, h=10);
    }
}

module arduino_case()
{
    difference()
    {
    translate ([ - thickness , 0, 0 ]) color ([1,1,0]) 
    cube ([arduino_width + thickness * 2  , arduino_length + 2, thickness * 1.8]); 
    translate ([ 2.5,0,0]) color ([0,1,0]) cube ([13 , arduino_length + 2, thickness * 1.8]); 
    }
}

module arduino_card()
{
  color ([1,0,0]) translate ([ 0 , 0, 0 ]) cube ([arduino_width, arduino_length, thickness * 3]);     
}

module box()
{
    difference()
    {
        union()
        {
            raw_plate();
            sides();
            screw_cylinders();
            arduino();
            color ([ 0,1,0]) translate ([0,0,thickness]) switch_support_pads();
            bars();
            
        }
        jack_hole();
        screw_holes();
        cable_hole();   
    }
}

module cable_hole()
{
    color ([1,0,0])
    translate ([length /2, width, height - cable_diam / 2 - 0.1]) 
    rotate ([90,0,0]) cylinder (r = cable_diam / 2, h = thickness);
    

    color ([0,1,0]) 
    translate ([length / 2 - cable_diam / 2, width - thickness, height - cable_diam / 2 - 0.1])
    cube ([ cable_diam, thickness, cable_diam /2 + 0.1]);
}

module screw_cylinders()
{
    translate ([ thickness + thickness / 2, thickness + thickness / 2, 0]) screw();
    translate ([ length - thickness - thickness / 2, thickness + thickness / 2, 0]) screw();
    translate ([ thickness + thickness / 2, width - thickness - thickness / 2, 0]) screw();
    translate ([ length - thickness - thickness / 2, width - thickness - thickness / 2, 0]) screw();
}

module screw_holes()
{
    translate ([ thickness + thickness / 2, thickness + thickness / 2, 0]) screw_hole();
    translate ([ length - thickness - thickness / 2, thickness + thickness / 2, 0]) screw_hole();
    translate ([ thickness + thickness / 2, width - thickness - thickness / 2, 0]) screw_hole();
    translate ([ length - thickness - thickness / 2, width - thickness - thickness / 2, 0]) screw_hole();
}

module screw()
{
    cylinder (h=height, r=(screw_cylinder / 2));
}

module screw_hole()
{
    translate([0, 0, thickness]) cylinder (h=height- thickness, r=(screw_cylinder_inside / 2));
}

module sides()
{
    sides_width();
    translate ([length - thickness, 0, 0]) sides_width();    

    sides_length();
    translate ([0, width - thickness, 0]) sides_length();

}

module bars()
{
    bar_diam = 5;
    bar_ray = bar_diam / 2;
    delta = cable_diam / 2 + bar_ray;
    translate([delta - 10,10,0]) cable_bar(bar_ray);
    translate([-delta - 10,10,0]) cable_bar(bar_ray);

    translate([delta + 15,10,0]) cable_bar(bar_ray);
    translate([-delta + 15,10,0]) cable_bar(bar_ray);
}

module cable_bar(bar_ray = 0)
{
    translate([length / 2, width / 2 , 0])
    difference()
    {
    cylinder (r=bar_ray, h=height);
    cylinder (r=bar_ray - 1, h=height);
    }
}

module sides_width(){
    cube ([thickness, width, height]);
}

module sides_length(){
    cube ([length, thickness, height]);
}

module plate()
{
    translate ([0, 120,0])
    difference()
    {
        upper_plate();
        screw_holes();
    }
}

module clipping_parts()
{   
    plot_width = 10;
    color ([1,0,0])
    union()
    {
        step = length / 5;
        //high row
        plot(step , width - 2 * thickness, thickness, plot_width);
        //plot(4 * step, width - 2 * thickness, thickness);
        plot(4 * step - plot_width, width - 2 * thickness, thickness, plot_width);  
        
        //low row
        plot(step, thickness, thickness, plot_width);
        //plot(4 * step, thickness, thickness);
        plot(4 * step - plot_width, thickness, thickness, plot_width);
    }
    
    color ([1,0,0])
    rotate ([0,0,90])
    union()
    {
        step = length / 3;
        //Left
        plot(step - plot_width / 2, - 2 * thickness , thickness, plot_width);
        plot(step - plot_width / 2, - length + thickness , thickness, plot_width);
    }    
}

module plot(x ,y, z, plot_width)
{
    plot_height = 3;
    translate ([x ,y, z])
    cube ([plot_width, thickness, plot_height]);
}
module plate_screw_holes()
{
    translate ([ thickness + thickness / 2, thickness + thickness / 2, 0]) plate_screw_hole();
    translate ([ length - thickness - thickness / 2, thickness + thickness / 2, 0]) plate_screw_hole();
    translate ([ thickness + thickness / 2, width - thickness - thickness / 2, 0]) plate_screw_hole();
    translate ([ length - thickness - thickness / 2, width - thickness - thickness / 2, 0]) plate_screw_hole();
}

module plate_screw_hole()
{
    cylinder (h=height, r=(diam_plate_screw_hole / 2));
}

module oled_screen()
{
    oled_width = 27.5;
    oled_height = 11.5;
    
    oled_width2 = 40.5;
    oled_height2 = 14;
    
    x_translation = length / 2 - oled_width / 2;
    y_translation = 80 - switch / 2 ;
    
    
    translate ([x_translation, y_translation, 0]) 
    color ([1, 0, 0 ]) cube ([oled_width, oled_height, thickness]);
    
    x_translation2 = length / 2 - oled_width2 / 2;
    y_translation2 = 80 - switch / 2 - (oled_height2 - oled_height) / 4;
    translate ([x_translation2, y_translation2, 1]) 
    color ([0, 1, 0 ]) cube ([oled_width2, 12.8, thickness]);
}

module raw_plate()
{
    cube ([length, width, thickness]);
}

module switches_holes()
{
  // lower row
  translate ([20,20,0]) cylinder (h=thickness, r=(switch /2));
  translate ([120,20,0]) cylinder (h=thickness, r=(switch /2));
  
  // higher row  
  translate ([20,80,0]) cylinder (h=thickness, r=(switch /2));
  translate ([120,80,0]) cylinder (h=thickness, r=(switch /2));
}

module switch_support_pads()
{
 
  // higher row  
  translate ([20,80,0]) cylinder (h=height - 19.5, r=2);
  translate ([120,80,0]) cylinder (h=height - 19.5, r=2);
}

module upper_plate()
{
        difference()
        {
            raw_plate();
            color ([1,0,0]) switches_holes();
            plate_screw_holes();
            oled_screen();
        }
}

